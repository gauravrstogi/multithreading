//
//  ViewController.m
//  MultiThreadSample
//
//  Created by Gaurav Rastogi on 03/07/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onClickOfShowImageButton {
    
    [self performSelectorInBackground:@selector(aSampleMethodToShowImageUsingPerformSelector:) withObject:@"Chirag"];
}
- (IBAction)onClickOfShowImageButtonUsingGCD {
    
    [self aSampleMethodToShowImageUsingGCD];
}


-(void)aSampleMethodToShowImageUsingGCD
{
    __weak ViewController *weakSelf = self;
    
    dispatch_queue_t userDefinedQueue = dispatch_queue_create("ThisIsMyCustomQueue", DISPATCH_QUEUE_SERIAL);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://live-wallpaper.net/iphone5/img/app/i/p/iphone-5-wallpapers-hd-3754-f3p93_3d1b49c941327fb9fd60b4a330cc07fc_raw.jpg"]]];
        
        NSLog(@"The Block is getting executed");
        dispatch_sync(dispatch_get_main_queue(), ^{
            [weakSelf.imgView setImage:image];
        });
        
        dispatch_async(userDefinedQueue, ^{
            NSLog(@"User defined is performing task");
        });
        
    });
}


-(void)aSampleMethodToShowImageUsingPerformSelector:(NSString *)string
{
    NSLog(@"A Sample Methods for Demonstration : %@",string);
    
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://live-wallpaper.net/iphone5/img/app/i/p/iphone-5-wallpapers-hd-3754-f3p93_3d1b49c941327fb9fd60b4a330cc07fc_raw.jpg"]]];
    
    //[self.imgView setImage:image];
    [self performSelectorOnMainThread:@selector(setImgOnImgView:) withObject:image waitUntilDone:NO];
}

-(void)setImgOnImgView:(UIImage *)image
{
    [self.imgView setImage:image];
}

- (IBAction)onClickOfButton {
    
    NSLog(@"Click Me button has been clicked");
    
    
}





@end
