//
//  AppDelegate.h
//  MultiThreadSample
//
//  Created by Gaurav Rastogi on 03/07/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

