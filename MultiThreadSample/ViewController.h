//
//  ViewController.h
//  MultiThreadSample
//
//  Created by Gaurav Rastogi on 03/07/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)onClickOfButton;

@end

